﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace navegapag
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //Metodo de programacion del slider

        void OnSliderValueChanged(object sender, ValueChangedEventArgs args)
        {
            // luego de crearlo voy a xaml y en slider pongo valuechanged con "ValueChangedEventArgs"

            /*if (sender == id_sl_blue)
            {
                DisplayAlert("Error", "Prueba de colores", "OK");
            }*/

            //  apartir del color me trae el valor en Hexadecimal, para que me reotrne el valor lo paso a los colores primarios
            //  traigo el id con lo que cree el boxview y le doy = y se lo asigno

            id_bv_background.BackgroundColor = Color.FromRgba((int)id_sl_red.Value, (int)id_sl_green.Value, (int)id_sl_blue.Value, (int)id_sl_alpha.Value);


            if (sender == id_sl_red)
            {
                number_total.Text = "" + (int)id_sl_red.Value;
            }
            else if (sender == id_sl_blue)
            {
                number_total2.Text = "" + (int)id_sl_blue.Value;
            }
            else if (sender == id_sl_green)
            {
                number_total1.Text = "" + (int)id_sl_green.Value;
            }
            else if (sender == id_sl_alpha)
            {
                number_total3.Text = "" + (int)id_sl_alpha.Value;
            }

        }

        async private void ButtonCliked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

    }
}
