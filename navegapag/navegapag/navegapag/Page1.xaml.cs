﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace navegapag
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        void CuadroMovimiento(object sender, ValueChangedEventArgs args)
        {

            if (sender == id_higth)
            {
                id_cuadrado.HeightRequest = id_higth.Value;
             
            }
            else if (sender == id_width)
            {
                id_cuadrado.WidthRequest = id_width.Value;
               
            }

        }

    }
}